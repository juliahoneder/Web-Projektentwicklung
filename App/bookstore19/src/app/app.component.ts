import {Component} from '@angular/core';
import {Book} from "./shared/book";
import {AuthService} from "./shared/authentication.service";

@Component({
    selector: 'bs-root',
    templateUrl: './app.component.html',
    styles: []
})
export class AppComponent {
    listOn = true;
    detailsOn = false;

    book: Book;

    constructor(private authService: AuthService) {

    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }

    isAdmin() {
        return localStorage.getItem('isAdmin') == '1';
    }

    getLoginLabel() {
        if (this.isLoggedIn()) {
            return "Logout"
        }
        else {
            return "Login"
        }
    }
}