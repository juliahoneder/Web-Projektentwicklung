import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {BookListComponent} from './book/book-list/book-list.component';
import {BookListItemComponent} from './book/book-list-item/book-list-item.component';
import {BookDetailsComponent} from './book/book-details/book-details.component';
import {BookFormComponent} from "./book/book-form/book-form.component";
import {SearchComponent} from './search/search.component';
import {LoginComponent} from "./login/login.component";
import {BookService} from "./shared/book.service";
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {DateValueAccessorModule} from "angular-date-value-accessor";
import {AuthService} from "./shared/authentication.service";
import {TokenInterceptorService} from "./shared/token-interceptor.service";
import {JwtInterceptorService} from "./shared/jwt-interceptor.service";
import {OrderListComponent} from './order/order-list/order-list.component';
import {OrderAdminComponent} from './order/order-admin/order-admin.component';
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component';
import {OrderListItemComponent} from './order/order-list-item/order-list-item.component';
import {OrderService} from "./shared/order.service";
import {OrderDetailsComponent} from './order/order-details/order-details.component';
import {registerLocaleData} from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import {OrderAdminItemComponent} from './order/order-admin-item/order-admin-item.component';
import { ShoppingCartCheckoutComponent } from './shopping-cart-checkout/shopping-cart-checkout.component';

registerLocaleData(localeDe, 'de-DE', localeDeExtra);

@NgModule({
    // lists the components that build the module
    declarations: [
        AppComponent,
        BookListComponent,
        BookListItemComponent,
        BookDetailsComponent,
        BookFormComponent,
        HomeComponent,
        SearchComponent,
        LoginComponent,
        OrderAdminComponent,
        OrderListComponent,
        OrderListItemComponent,
        OrderDetailsComponent,
        ShoppingCartComponent,
        OrderAdminItemComponent,
        ShoppingCartCheckoutComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        DateValueAccessorModule
    ],
    // lists the services that are provided
    providers: [
        BookService,
        OrderService,
        AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptorService,
            multi: true
        },
        {
            provide: LOCALE_ID,
            useValue: 'de-DE'
        }
    ],
    // sets start component
    bootstrap: [AppComponent]
})
export class AppModule {
}