import {Component, OnInit} from '@angular/core';
import {AuthService} from "../shared/authentication.service";
import {OrderService} from "../shared/order.service";
import {Address} from "../shared/address";

@Component({
    selector: 'bs-shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styles: []
})
export class ShoppingCartComponent implements OnInit {

    books = [];
    addresses: Address[];
    sum_gross = 0;

    constructor(public os: OrderService,
                public authService: AuthService) {
    }

    ngOnInit() {
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            if (key.slice(0, 5) === 'book_') {
                let book = JSON.parse(localStorage.getItem(key));
                this.sum_gross += book.price_net * 1.1 * +this.getAmount(book);
                this.books.push(book);
            }
        }

        if (this.authService.isLoggedIn()) {
            this.os.getAddresses(localStorage.getItem('userId')).subscribe((res) => {
                this.addresses = res;

                if (localStorage.getItem('billing_address') === null) {
                    this.setBillingAddress(this.addresses[0]);
                }

                if (localStorage.getItem('shipping_address') === null) {
                    this.setShippingAddress(this.addresses[0]);
                }
            });
        }
    }

    getAmount(book) {
        return localStorage.getItem('amount_book_' + book.isbn);
    }

    decreaseAmount(book) {
        let amount = +this.getAmount(book);
        if (amount >= 2) {
            amount--;
            localStorage.setItem('amount_book_' + book.isbn, '' + amount);
            this.sum_gross -= book.price_net * 1.1;
        }
    }

    increaseAmount(book) {
        let amount = +this.getAmount(book);
        amount++;
        localStorage.setItem('amount_book_' + book.isbn, '' + amount);
        this.sum_gross += book.price_net * 1.1;
    }

    removeBookFromShoppingCart(book) {
        localStorage.removeItem('book_' + book.isbn);
        this.sum_gross -= book.price_net * 1.1 * +this.getAmount(book);
        document.getElementById(book.isbn).remove();
    }

    checkBillingAddress(address) {
        return localStorage.getItem('billing_address').toLowerCase() === JSON.stringify(address).toLowerCase();
    }

    checkShippingAddress(address){
        return localStorage.getItem('shipping_address').toLowerCase() === JSON.stringify(address).toLowerCase();
    }

    setBillingAddress(address) {
        localStorage.setItem('billing_address', JSON.stringify(address));
    }

    setShippingAddress(address){
        localStorage.setItem('shipping_address', JSON.stringify(address));
    }
}