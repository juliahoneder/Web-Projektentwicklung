import {Order} from "./order";

export class OrderFactory {
    static empty(): Order {
        return new Order(null, null, null, null, new Date(), 0,0);
    }

    static fromObject(rawOrder: any): Order {
        return new Order(
            rawOrder.id,
            rawOrder.user_id,
            rawOrder.billing_address,
            rawOrder.shipping_address,
            typeof(rawOrder.date) === 'string' ?
                new Date(rawOrder.date) : rawOrder.date,
            rawOrder.price_total_net,
            rawOrder.price_total_gross
        );
    }
}