export class Status {
    constructor(public id: number, public order_id: number, public date: Date, public status: string, public comment?: string) {
    }
}