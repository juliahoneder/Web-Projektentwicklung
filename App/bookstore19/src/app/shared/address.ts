export class Address {
    constructor(public id: number, public firstname: string, public lastname: string, public street: string,
                public number: string, public zip: string, public city: string, public country: string) {
    }
}