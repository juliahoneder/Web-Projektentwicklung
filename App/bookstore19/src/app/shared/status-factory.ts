import {Status} from "./status";

export class StatusFactory {
    static empty(): Status {
        return new Status(null, null, new Date(), 'open', '');
    }

    static fromObject(rawStatus: any): Status {
        return new Status(
            rawStatus.id,
            rawStatus.order_id,
            typeof(rawStatus.date) === 'string' ?
                new Date(rawStatus.date) : rawStatus.date,
            rawStatus.status,
            rawStatus.comment
        );
    }
}
