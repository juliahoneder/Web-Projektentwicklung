import {Component, OnInit} from '@angular/core';
import {Book} from "../../shared/book";
import {BookService} from "../../shared/book.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BookFactory} from "../../shared/book-factory";
import {AuthService} from "../../shared/authentication.service";

@Component({
    selector: 'bs-book-details',
    templateUrl: './book-details.component.html',
    styles: []
})
export class BookDetailsComponent implements OnInit {

    book: Book = BookFactory.empty();

    constructor(private bs: BookService,
                private route: ActivatedRoute,
                private router: Router,
                public authService: AuthService) {
    }

    ngOnInit() {
        const params = this.route.snapshot.params;
        this.bs.getSingle(params['isbn']).subscribe(res => this.book = res);
    }

    getRating(num: number) {
        return new Array(num);
    }

    addBookToShoppingCart() {
        localStorage.setItem('book_' + this.book.isbn, JSON.stringify(this.book));
        localStorage.setItem('amount_book_' + this.book.isbn, '1');
    }

    deleteBook() {
        if (confirm('Do you really want to delete this book?')) {
            this.bs.delete(this.book.isbn).subscribe(res => this.router.navigate(['../'], {relativeTo: this.route}));
        }
    }
}