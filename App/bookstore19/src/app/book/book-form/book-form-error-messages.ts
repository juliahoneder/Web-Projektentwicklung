export class ErrorMessage {
    constructor(
        public forControl: string,
        public forValidator: string,
        public text: string) {
    }
}

export const BookFormErrorMessages = [
    new ErrorMessage('title', 'required', 'A book title has to be specified'),
    new ErrorMessage('authors', 'atLeastOneAuthor', 'At least one author has to be specified'),
    new ErrorMessage('isbn', 'required', 'An ISBN has to be specified'),
    new ErrorMessage('isbn', 'minlength', 'The ISBN has to contain at least 10 characters'),
    new ErrorMessage('isbn', 'maxlength', 'The ISBN can not exceed 20 characters'),
    new ErrorMessage('isbn', 'isbnFormat', 'The ISBN has to contain 10 or 13 numbers'),
    new ErrorMessage('published', 'required', 'A publication date has to be specified'),
    new ErrorMessage('rating', 'min', 'Rating can only accept positive values'),
    new ErrorMessage('rating', 'max', 'Maximum 10 stars allowed'),
    new ErrorMessage('price_net', 'required', 'A net price has to be specified'),
    new ErrorMessage('images', 'atLeastOneImage', 'At least one image has to be specified')
];