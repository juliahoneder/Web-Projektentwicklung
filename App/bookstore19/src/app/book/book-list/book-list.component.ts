import {Component, OnInit} from '@angular/core';
import {Book} from "../../shared/book";
import {BookService} from "../../shared/book.service";

@Component({
    selector: 'bs-book-list',
    templateUrl: './book-list.component.html',
    styles: []
})
export class BookListComponent implements OnInit {

    books: Book[];

    // dependency injection
    constructor(private bs: BookService) {
    }

    ngOnInit() {
        this.bs.getAll().subscribe(res => this.books = res);
    }
}