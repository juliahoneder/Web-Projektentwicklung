export class ErrorMessage {
    constructor(
        public forControl: string,
        public forValidator: string,
        public text: string) {
    }
}

export const OrderDetailsErrorMessages = [
    new ErrorMessage('status', 'required', 'A status has to be specified'),
];