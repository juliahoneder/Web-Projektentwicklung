import {Component, OnInit} from '@angular/core';
import {Order} from "../../shared/order";
import {OrderService} from "../../shared/order.service";

@Component({
    selector: 'bs-order-list',
    templateUrl: './order-list.component.html',
    styles: []
})
export class OrderListComponent implements OnInit {

    orders: Order[];

    constructor(private os: OrderService) {
    }

    ngOnInit() {
        this.os.getAllByUserId(localStorage.getItem('userId')).subscribe(res => this.orders = res);
    }
}