import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {debounceTime, distinctUntilChanged, switchMap, tap} from "rxjs/internal/operators";
import {BookService} from "../shared/book.service";
import {Book} from "../shared/book";
import {syntaxError} from "@angular/compiler";

@Component({
    selector: 'bs-search',
    templateUrl: './search.component.html',
    styles: []
})
export class SearchComponent implements OnInit {

    @Output() bookSelected = new EventEmitter<Book>();
    keyup = new EventEmitter<string>();
    foundBooks: Book[] = [];
    isLoading = false;

    constructor(private bs: BookService) {
    }

    ngOnInit() {
        this.keyup
            .pipe(debounceTime(500))
            .pipe(distinctUntilChanged())
            .pipe(switchMap((searchTerm) => this.bs.getAllSearch(searchTerm)))
            .pipe(tap(() => this.isLoading = true))
            .subscribe((books) => {
                this.foundBooks = books;
            });
    }
}