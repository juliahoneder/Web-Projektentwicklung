<?php

use App\Http\Controllers\BookController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('home/{amount}', 'BookController@getNewReleases');
Route::get('books', 'BookController@index');
Route::get('book/{isbn}', 'BookController@bookByISBN');
Route::get('book/checkisbn/{isbn}', 'BookController@checkISBN');
Route::get('book/search/{searchTerm}', 'BookController@findSearchTerm');

Route::group(['middleware' => ['api', 'cors', 'jwt.auth']], function () {
    Route::get('myorders/{userId}', 'OrderController@ordersByUserId');
    Route::get('myorder/{id}', 'OrderController@orderById');
    Route::get('addresses/{userId}', 'AddressController@addressesByUserId');
    Route::post('myorder', 'OrderController@saveOrder');
    Route::post('auth/logout', 'Auth\ApiAuthController@logout');
});

Route::group(['middleware' => ['api', 'cors', 'jwt.auth', 'auth.admin']], function () {
    Route::post('book', 'BookController@save');
    Route::put('book/{isbn}', 'BookController@update');
    Route::delete('book/{isbn}', 'BookController@delete');
    Route::get('myorders', 'OrderController@index');
    Route::post('status', 'OrderController@saveStatus');
});

Route::group(['middleware' => ['api', 'cors']], function () {
    Route::post('auth/login', 'Auth\ApiAuthController@login');
    Route::post('auth/register', 'Auth\ApiRegisterController@create');
});