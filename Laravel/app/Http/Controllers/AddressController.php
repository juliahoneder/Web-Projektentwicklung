<?php

namespace App\Http\Controllers;

use App\Address;

class AddressController extends Controller
{
    public function addressesByUserId(string $user_id) {
        $addresses = Address::where('user_id', $user_id)->get();
        return $addresses;
    }
}