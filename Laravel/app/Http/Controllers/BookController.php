<?php

namespace App\Http\Controllers;

use App\Book;
use App\Image;
use App\Author;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function index()
    {
        // load all books and relations with eager loading
        $books = Book::with(['authors', 'images', 'user'])->orderBy('title')
            ->where('deleted', 0)
            ->get();
        return $books;
    }

    public function bookByISBN(string $isbn)
    {
        $book = Book::where('isbn', $isbn)->with(['authors', 'images', 'user'])
            ->where('deleted', 0)
            ->first();
        return $book;
    }

    public function checkISBN(string $isbn)
    {
        $book = Book::where('isbn', $isbn)->first();
        return $book != null ?
            response()->json('book with ' . $isbn . ' exists', 200) :
            response()->json('book with ' . $isbn . ' doesn\'t exists', 404);
    }

    /**
     * find book by search term
     * SQL injection is prevented by default, because Eloquent
     * uses PDO parameter binding
     */
    public function findSearchTerm(string $searchTerm)
    {
        $book = Book::with(['authors', 'images', 'user'])
            ->where('title', 'LIKE', '%' . $searchTerm . '%')
            ->where('deleted', 0)
            ->orWhere('subtitle', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('description', 'LIKE', '%' . $searchTerm . '%')
            /* search term in authors name */
            ->orWhereHas('authors', function ($query) use ($searchTerm) {
                $query->where('firstname', 'LIKE', '%' . $searchTerm . '%')
                    ->orWhere('lastname', 'LIKE', '%' . $searchTerm . '%');
            })->get();
        return $book;
    }

    public function getNewReleases(string $amount) {
        $books = Book::with(['authors', 'images', 'user'])
            ->where('deleted', 0)
            ->orderBy('published', 'desc')
            ->take($amount)
            ->get();
        return $books;
    }

    /**
     * save new book
     */
    public function save(Request $request): JsonResponse
    {
        $request = $this->parseRequest($request);

        // use a transaction for saving model including relations
        // if one query fails, complete SQL statements will be rolled back
        DB::beginTransaction();

        try {
            $book = Book::create($request->all());

            // save images
            if (isset($request['images']) && is_array($request['images'])) {
                foreach ($request['images'] as $img) {
                    $image = Image::firstOrNew(['url' => $img['url'], 'title' => $img['title']]);
                    $book->images()->save($image);
                }
            }

            // save authors
            if (isset($request['authors']) && is_array($request['authors'])) {
                foreach ($request['authors'] as $auth) {
                    $author = Author::firstOrNew(['firstname' => $auth['firstname'], 'lastname' => $auth['lastname']]);
                    $book->authors()->save($author);
                }
            }

            DB::commit();

            // return a vaild http response
            return response()->json($book, 201);

        } catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("saving book failed: " . $e->getMessage(), 420);
        }
    }

    /**
     * update existing book
     */
    public function update(Request $request, string $isbn): JsonResponse
    {
        DB::beginTransaction();

        try {
            $book = Book::with(['authors', 'images', 'user'])
                ->where('isbn', $isbn)->first();
            if ($book != null) {
                $request = $this->parseRequest($request);
                $book->update($request->all());

                //delete all old images
                $book->images()->delete();

                // save images
                if (isset($request['images']) && is_array($request['images'])) {
                    foreach ($request['images'] as $img) {
                        $image = Image::firstOrNew(['url' => $img['url'], 'title' => $img['title']]);
                        $book->images()->save($image);
                    }
                }

                //update authors
                if (isset($request['authors']) && is_array($request['authors'])) {
                    $ids = [];

                    foreach ($request['authors'] as $auth) {
                        $authorId = Author::where('firstname', $auth['firstname'])->where('lastname', $auth['lastname'])->first();

                        if (!isset($authorId['id'])) {
                            $author = Author::firstOrNew(['firstname' => $auth['firstname'], 'lastname' => $auth['lastname']]);
                            $book->authors()->save($author);
                            array_push($ids, $author['id']);
                        }
                        else {
                            array_push($ids, $authorId['id']);
                        }
                    }

                    $book->authors()->sync($ids);
                }

                $book->save();
            }

            DB::commit();

            $book1 = Book::with(['authors', 'images', 'user'])->where('isbn', $isbn)->first();

            // return a vaild http response
            return response()->json($book1, 201);

        } catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("updating book failed: " . $e->getMessage(), 420);
        }
    }

    /**
     * returns 200 if book deleted successfully, throws exception if not
     */
    public function delete(string $isbn): JsonResponse
    {
        try {
            $book = Book::where('isbn', $isbn)->update(array('deleted' => 1));

            return response()->json('book (' . $isbn . ') successfully deleted', 200);

        } catch (\Exception $e) {
            return response()->json("deleting book failed: " . $e->getMessage(), 420);
        }
    }

    /**
     * modify / convert values if needed
     */
    private function parseRequest(Request $request): Request
    {
        // get date and convert it - its in ISO 8601, e.g. "2018-01-01T23:00:00.000Z"
        $date = new \DateTime($request->published);
        $request['published'] = $date;
        return $request;
    }
}
