<?php

use App\Address;
use App\Order;
use App\Book;
use App\Status;
use App\User;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order1 = new Order();
        $order1->date = new DateTime('2019-04-17 22:15');
        $order1->price_total_net = 213.00;
        $order1->price_total_gross = 234.30;

        $user1 = User::all()->first();
        $order1->user()->associate($user1);

        $billingAddress1 = Address::all()->get('id', 1);
        $order1->billingAddress()->associate($billingAddress1);

        $shippingAddress1 = Address::all()->get('id', 2);
        $order1->shippingAddress()->associate($shippingAddress1);

        $order1->save();

        $books1 = Book::all()->pluck('id');
        $order1->books()->attach($books1, ['amount' => 3]);
        $order1->save();

        // -------------------------------------------------------------------------------

        $order2 = new Order();
        $order2->date = new DateTime('2019-04-18 08:41');
        $order2->price_total_net = 71.00;
        $order2->price_total_gross = 87.10;

        // gets first user
        $user2 = User::all()->get('id', 2);
        $order2->user()->associate($user2);

        $billingAddress2 = Address::all()->get('id', 3);
        $order2->billingAddress()->associate($billingAddress2);

        $shippingAddress2 = Address::all()->get('id', 3);
        $order2->shippingAddress()->associate($shippingAddress2);

        $order2->save();

        $books2 = Book::all()->pluck('id');;
        $order2->books()->sync($books2);

        $order2->save();

        // -------------------------------------------------------------------------------

        $order3 = new Order();
        $order3->date = new DateTime('2019-04-22 11:53');
        $order3->price_total_net = 71.00;
        $order3->price_total_gross = 87.10;

        // gets first user
        $user3 = User::all()->get('id', 1);
        $order3->user()->associate($user3);

        $billingAddress3 = Address::all()->get('id', 2);
        $order3->billingAddress()->associate($billingAddress3);

        $shippingAddress3 = Address::all()->get('id', 2);
        $order3->shippingAddress()->associate($shippingAddress3);

        $order3->save();

        $books3 = Book::all()->pluck('id');;
        $order3->books()->sync($books3);

        $order3->save();

        // -------------------------------------------------------------------------------

        $order4 = new Order();
        $order4->date = new DateTime('2019-04-21 19:33');
        $order4->price_total_net = 115.39;
        $order4->price_total_gross = 126.39;

        // gets first user
        $user4 = User::all()->get('id', 2);
        $order4->user()->associate($user4);

        $billingAddress4 = Address::all()->get('id', 3);
        $order4->billingAddress()->associate($billingAddress4);

        $shippingAddress4 = Address::all()->get('id', 3);
        $order4->shippingAddress()->associate($shippingAddress4);

        $order4->save();

        $books41 = Book::all()->pluck('id')->first();
        $books42 = Book::all()->pluck('id')->last();
        $books43 = Book::all()->pluck('id')->get('id', 2);
        $order4->books()->attach($books41, ['amount' => 2]);
        $order4->books()->attach($books42, ['amount' => 3]);
        $order4->books()->attach($books43);

        $order4->save();
    }
}
