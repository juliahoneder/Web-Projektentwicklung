<?php

use App\Author;
use App\Book;
use App\Image;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert
        $book1 = new Book();
        $book1->isbn = '978-3-551-35401-3';
        $book1->title = 'Harry Potter and the Philosopher\'s Stone';
        $book1->subtitle = 'Volume 1';
        $book1->description = 'Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry\'s eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!';
        $book1->published = '2005.01.25';
        $book1->rating = 10;
        $book1->price_net = 14.30;

        // gets first user
        $user = User::all()->first();
        $book1->user()->associate($user);

        $book1->save();

        // adds authors
        $authors = Author::all()->pluck('id');
        $book1->authors()->sync($authors);

        // adds images
        $image11 = new Image();
        $image11->title = 'Cover 1';
        $image11->url = 'https://images-na.ssl-images-amazon.com/images/I/51ifu1aebKL._SX332_BO1,204,203,200_.jpg';
        $image11->book()->associate($book1);
        $image11->save();

        // adds images
        $image12 = new Image();
        $image12->title = 'Description';
        $image12->url = 'https://images-na.ssl-images-amazon.com/images/I/51kdIBI0MlL.jpg';
        $image12->book()->associate($book1);
        $image12->save();

        $book1->save();

        // -------------------------------------------------------------------------------

        $book2 = new Book;
        $book2->isbn = '978-3-551-35402-0';
        $book2->title = 'Harry Potter and the Chamber of Secrets';
        //$book2->author = 'Joanne K. Rowling';
        $book2->description = 'Harry Potter\'s summer has included the worst birthday ever, doomy warnings from a house-elf called Dobby, and rescue from the Dursleys by his friend Ron Weasley in a magical flying car! Back at Hogwarts School of Witchcraft and Wizardry for his second year, Harry hears strange whispers echo through empty corridors - and then the attacks start. Students are found as though turned to stone ...Dobby\'s sinister predictions seem to be coming true.';
        $book2->published = '2006.02.23';
        $book2->rating = 7;
        $book2->price_net = 10.30;

        // gets first user
        $book2->user()->associate($user);

        $book2->save();

        // adds authors
        $book2->authors()->sync($authors);

        // adds images
        $image21 = new Image();
        $image21->title = 'Cover 1';
        $image21->url = 'https://images-na.ssl-images-amazon.com/images/I/51F7MMxbhOL._SX324_BO1,204,203,200_.jpg';
        $image21->book()->associate($book2);
        $image21->save();

        // adds images
        $image22 = new Image();
        $image22->title = 'Description';
        $image22->url = 'https://images-na.ssl-images-amazon.com/images/I/5191isFAtRL.jpg';
        $image22->book()->associate($book2);
        $image22->save();

        $book2->save();

        // -------------------------------------------------------------------------------

        $book3 = new Book;
        $book3->isbn = '978-3-551-35403-7';
        $book3->title = 'Harry Potter and the Prisoner of Azkaban';
        $book3->description = 'When the Knight Bus crashes through the darkness and screeches to a halt in front of him, it\'s the start of another far from ordinary year at Hogwarts for Harry Potter. Sirius Black, escaped mass-murderer and follower of Lord Voldemort, is on the run - and they say he is coming after Harry. In his first ever Divination class, Professor Trelawney sees an omen of death in Harry\'s tea leaves ... But perhaps most terrifying of all are the Dementors patrolling the school grounds, with their soul-sucking kiss.';
        $book3->published = '2007.02.23';
        $book3->rating = 8;
        $book3->price_net = 20.60;

        // gets first user
        $book3->user()->associate($user);

        $book3->save();

        // adds authors
        $book3->authors()->sync($authors);

        // adds images
        $image31 = new Image();
        $image31->title = 'Cover 1';
        $image31->url = 'https://images-na.ssl-images-amazon.com/images/I/51t55KcM8pL._SX332_BO1,204,203,200_.jpg';
        $image31->book()->associate($book3);
        $image31->save();

        // adds images
        $image32 = new Image();
        $image32->title = 'Description';
        $image32->url = 'https://images-na.ssl-images-amazon.com/images/I/41co-BeJwIL.jpg';
        $image32->book()->associate($book3);
        $image32->save();

        $book3->save();


        // -------------------------------------------------------------------------------

        $book4 = new Book;
        $book4->isbn = '978-3-551-35404-4';
        $book4->title = 'Harry Potter and the Goblet of Fire';
        $book4->subtitle = 'Volume 4';
        $book4->description = 'The Triwizard Tournament is to be held at Hogwarts. Only wizards who are over seventeen are allowed to enter - but that doesn\'t stop Harry dreaming that he will win the competition. Then at Hallowe\'en, when the Goblet of Fire makes its selection, Harry is amazed to find his name is one of those that the magical cup picks out. He will face death-defying tasks, dragons and Dark wizards, but with the help of his best friends, Ron and Hermione, he might just make it through - alive!';    $book4->published = '2008.12.02';
        $book4->rating = 10;
        $book4->price_net = 25.80;

        // gets first user
        $book4->user()->associate($user);

        $book4->save();

        // adds authors
        $book4->authors()->sync($authors);

        // adds images
        $image41 = new Image();
        $image41->title = 'Cover 1';
        $image41->url = 'https://images-na.ssl-images-amazon.com/images/I/516SzpxSeML._SX332_BO1,204,203,200_.jpg';
        $image41->book()->associate($book4);
        $image41->save();

        // adds images
        $image42 = new Image();
        $image42->title = 'Description';
        $image42->url = 'https://images-na.ssl-images-amazon.com/images/I/41Yer7NkOQL.jpg';
        $image42->book()->associate($book4);
        $image42->save();

        $book4->save();
    }
}