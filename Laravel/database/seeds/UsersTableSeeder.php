<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();
        $user1->firstname = 'Julia';
        $user1->lastname = 'Honeder';
        $user1->email = 'test1@gmail.com';
        $user1->password = bcrypt('secret');
        $user1->save();

        // -------------------------------------------------------------------------------

        $user2 = new User();
        $user2->firstname = 'Philipp';
        $user2->lastname = 'Preiser';
        $user2->email = 'test2@gmail.com';
        $user2->password = bcrypt('secret');
        $user2->save();

        // -------------------------------------------------------------------------------

        $user3 = new User();
        $user3->firstname = 'admin';
        $user3->lastname = 'user';
        $user3->email = 'admin@gmail.com';
        $user3->password = bcrypt('secret');
        $user3->is_admin = 1;
        $user3->save();
    }
}
