<?php

use App\Order;
use App\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status1 = new Status();
        $status1->date = new DateTime('2019-04-17 22:30');
        $status1->status = 'paid';

        $order1 = Order::all()->first();
        $status1->order()->associate($order1);

        $status1->save();

        // -------------------------------------------------------------------------------

        $status2 = new Status();
        $status2->date = new DateTime('2019-04-18 08:32');
        $status2->status = 'sent';

        $order2 = Order::all()->first();
        $status2->order()->associate($order2);

        $status2->save();

        // -------------------------------------------------------------------------------

        $status3 = new Status();
        $status3->date = new DateTime('2019-04-19 03:54');
        $status3->status = 'canceled';

        $order3 = Order::all()->last();
        $status3->order()->associate($order3);
        $status3->save();

        // -------------------------------------------------------------------------------

        $status4 = new Status();
        $status4->date = new DateTime('2019-04-19 06:14');
        $status4->comment = 'order was wrongfully canceled';
        $status4->status = 'open';

        $order4 = Order::all()->last();
        $status4->order()->associate($order4);
        $status4->save();

        // -------------------------------------------------------------------------------

        $status5 = new Status();
        $status5->date = new DateTime('2019-04-19 06:14');
        $status5->status = 'paid';

        $order5 = Order::all()->get('id', '2');
        $status5->order()->associate($order5);
        $status5->save();

        // -------------------------------------------------------------------------------

        $status6 = new Status();
        $status6->date = new DateTime('2019-04-19 06:14');
        $status6->status = 'open';

        $order6 = Order::all()->get('id', '3');
        $status6->order()->associate($order6);
        $status6->save();
    }
}